# luizbra.dev's dotfiles

## What I use

- [Raspberry Pi 4B](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/): Main Computer
- [Neovim](https://neovim.io/): Text editor
- [tmux](https://github.com/tmux/tmux/wiki): Terminal multiplexer
- [Spotifyd](https://github.com/Spotifyd/spotifyd): Spotify daemon
- [pash](https://github.com/dylanaraps/pash): Password manager

## Installation

    git clone --bare https://codeberg.org/luizbra/dotfiles "$HOME/.dotfiles"
    git --git-dir="$HOME/.dotfiles" --work-tree="$HOME" checkout --force
    git --git-dir="$HOME/.dotfiles" --work-tree="$HOME" config --local status.showUntrackedFiles no
