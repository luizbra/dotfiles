set number
"set relativenumber
set scrolloff=8
"set colorcolumn=80
set nowrap
set noswapfile
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set nohlsearch
set autochdir
set mouse=a
set clipboard=unnamedplus

let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
Plug 'morhetz/gruvbox'
Plug 'sheerun/vim-polyglot'
Plug 'ap/vim-css-color'
Plug 'yggdroot/indentline'
Plug 'chun-yang/auto-pairs'
Plug 'qpkorr/vim-renamer'
Plug 'mattn/emmet-vim'
Plug 'itchyny/lightline.vim'
Plug 'edkolev/tmuxline.vim'
call plug#end()

set background=dark
colorscheme gruvbox
highlight Normal ctermbg=NONE guibg=NONE

let g:user_emmete_mode = 'a'
let g:user_emmet_leader_key = '<C-E>'

let g:lightline = { 'colorscheme': 'gruvbox' }
let g:tmuxline_powerline_separators = 0
let g:tmuxline_preset = 'tmux'
let g:tmuxline_preset = {
            \'a'      : '[#S]',
            \'win'    : '#I:#W#F',
            \'cwin'   : '#I:#W#F',
            \'y'      : '#(playerctl metadata -p spotifyd -f "{{ title }} - {{ artist }}")',
            \'z'      : '%R',
            \'options': { 'status-justify': 'left' },
            \}
